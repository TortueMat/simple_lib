"""Setup for Simple Lib"""
from simple_lib import __version__


from setuptools import (
    setup,
    find_packages
)

setup(
    name='simple_lib',
    version=__version__,
    description='Python library for Proof of Concept.',
    author='Mathieu Tortuyaux',
    packages=find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests",
            "log",
            "log.*",
            "*.log",
            "*.log.*"
        ]
    ),
    install_requires=[
        'docker==2.5.1',
    ]
)